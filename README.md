# Journal to alertmanager

THIS IS A PROTOTYPE ! DO NOT USE EXCEPT FOR TESTING PURPOSES ! 

This script is a wrapper for journalctl for sending systemd journal logs to [prometheus alertmanager](https://www.prometheus.io/docs/alerting/latest/alertmanager/).

The idea is to filter logs to generate alerts. See [Lofolomo](https://gitlab.com/samuelbf/lofomolo/) for a wider overview and a demo.

## Dependencies

- php
- php-curl
- systemd

## License

This prototype is licensed under GPLv3 terms.
